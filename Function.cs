using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using AnyStuff.Database;
using Newtonsoft.Json;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace AnyStuff
{
    public class Function
    {
        private static AmazonDynamoDBClient client = new AmazonDynamoDBClient();
        private readonly string PaginationToken = "pagination_token";
        private readonly string PageSize = "page_size";
        private readonly string AccountId = "account_id";
        private readonly string ContentType = "Content-Type";
        private readonly string ApplicationJson = "application/json";
        private DynamoDB dynamoDB = new DynamoDB(client);

        public async Task<APIGatewayProxyResponse> Handler(APIGatewayProxyRequest request, ILambdaContext context)
        {
            string paginationToken;
            string accountId;
            int pageSize;
            IDictionary<string, string> queryStringParameters = request.QueryStringParameters ?? new Dictionary<string, string>();

            TryGetParameters(queryStringParameters, out paginationToken, out pageSize, out accountId);

            var body = await dynamoDB.GetPostsAsync(accountId, pageSize, paginationToken);

            return new APIGatewayProxyResponse
            {
                StatusCode = body.Data.Count > 0 ? (int)HttpStatusCode.OK : (int)HttpStatusCode.NoContent,
                Body = JsonConvert.SerializeObject(body),
                Headers = new Dictionary<string, string> { { ContentType, ApplicationJson } }
            };
        }

        void TryGetParameters(IDictionary<string, string> queryParameters, out string paginationToken, out int pageSize, out string accountId)
        {
            pageSize = 5;
            accountId = queryParameters.ContainsKey(AccountId) ? queryParameters[AccountId] : string.Empty;
            paginationToken = queryParameters.ContainsKey(PaginationToken) ? queryParameters[PaginationToken] : null;
            if (queryParameters.ContainsKey(PageSize)) {
                int.TryParse(queryParameters[PageSize], out pageSize);
            }
        }
    }
}
