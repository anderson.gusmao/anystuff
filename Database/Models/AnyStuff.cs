﻿using System;
using Amazon.DynamoDBv2.DataModel;
using Newtonsoft.Json;

namespace AnyStuff.Database.Models
{
    [DynamoDBTable("AnyStuff")]
    public class AnyStuff
    {
        [JsonIgnore]
        [DynamoDBHashKey("account_id")]
        public string AccountId { get; set; }
        [JsonIgnore]
        [DynamoDBRangeKey("timestamp")]
        public ulong Timestamp { get; set; }
        [JsonProperty("content")]
        [DynamoDBProperty("content")]
        public Content Content { get; set; }
    }
}
