﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace AnyStuff.Database.Models
{
    public class PagedResult
    {
        [JsonProperty("pagination_token")]
        public string PaginationToken { get; set; }
        [JsonProperty("data")]
        public IList<AnyStuff> Data { get; set; }
    }
}
