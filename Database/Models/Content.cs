﻿using Amazon.DynamoDBv2.DataModel;
using Newtonsoft.Json;

namespace AnyStuff.Database.Models
{
    public class Content
    {
        [JsonProperty("author")]
        [DynamoDBProperty("author")]
        public string Author { get; set; }
        [JsonProperty("text")]
        [DynamoDBProperty("text")]
        public string Text { get; set; }
    }
}
