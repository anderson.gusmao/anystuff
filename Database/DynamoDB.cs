﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using AnyStuff.Database.Models;
using Newtonsoft.Json.Linq;

namespace AnyStuff.Database
{
    public class DynamoDB
    {
        private readonly string HashKey = "account_id";
        private readonly string RangeKey = "timestamp";
        private readonly string TableName = "AnyStuff";
        private readonly string EmptyToken = "{}";
        private AmazonDynamoDBClient client;

        public DynamoDB(AmazonDynamoDBClient client)
        {
            this.client = client;
        }

        public string generatePaginationToken(string accountId, string timestamp)
        {
            return $"{{\"{HashKey}\":{{\"S\":\"{accountId}\"}},\"{RangeKey}\":{{\"N\":\"{timestamp}\"}}}}";
        }

        public async Task<PagedResult> GetPostsAsync(string accountId, int pageSize, string timestamp = null, CancellationToken cancellationToken = default)
        {
            DynamoDBContext context = new DynamoDBContext(client);
            var table = Table.LoadTable(client, TableName);

            Search search = table.Query(new QueryOperationConfig
            {
                Limit = pageSize,
                BackwardSearch = true,
                PaginationToken = timestamp != null ? generatePaginationToken(accountId, timestamp) : null,
                KeyExpression = new Expression
                {
                    ExpressionStatement = $"{HashKey} = :{HashKey}",
                    ExpressionAttributeValues = { { $":{HashKey}", accountId } }
                }
            });

            var documents = await search.GetNextSetAsync(cancellationToken);
            string token = TryGetToken(search.PaginationToken);
            return new PagedResult
            {
                PaginationToken = token,
                Data = documents.Select(doc => context.FromDocument<Models.AnyStuff>(doc)).ToList()
            };
        }

        private string TryGetToken(string json)
        {
            if (string.IsNullOrEmpty(json) || json.Equals(EmptyToken))
                return null;

            JObject result = JObject.Parse(json);
            var value = result[RangeKey]["N"];
            return value != null ? value.ToString() : null;
        }
    }
}
